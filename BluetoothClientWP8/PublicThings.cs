﻿using BluetoothClientWP8;
using BluetoothConnectionManager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO.IsolatedStorage;

static class PublicThings
{


    public static List<ElementListeDeCourses> ListeObjetsCourses = new List<ElementListeDeCourses>();
    public static void TrierListeDeCourses() 
    {
        
    }
    public static void ChargerListeCourses()
    {
        ListeObjetsCourses = new List<ElementListeDeCourses>();
        string ContenuIsolatedStorage = null;
        try
        {
            ContenuIsolatedStorage = (string)System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings["courses"];
            //MessageBox.Show(ContenuIsolatedStorage)
        }
        catch (Exception ex)
        {
            EcrireSetting("courses", "");
            ChargerListeCourses();
            return;
        }
        string[] TableauSplitSetting = ContenuIsolatedStorage.Split(';');
        if (TableauSplitSetting.Length >= 1 & !string.IsNullOrEmpty(TableauSplitSetting[0]))
        {
            for (int i = 0; i <= TableauSplitSetting.Length - 1; i++)
            {
                string[] TableauSplitThisElement = TableauSplitSetting[i].Split(',');
                ListeObjetsCourses.Add(new ElementListeDeCourses(i, TableauSplitThisElement[1], int.Parse(TableauSplitThisElement[0]), bool.Parse(TableauSplitThisElement[2])));
            }
        }
    }
    public static void SauvegarderListeCourses()
    {
        string StringAEcrire = "";
        for (int i = 0; i <= ListeObjetsCourses.Count - 1; i++)
        {
            string PointVirgule = "";
            if (i != ListeObjetsCourses.Count - 1)
            {
                PointVirgule = ";";
            }
            StringAEcrire += ListeObjetsCourses[i].ID + "," + ListeObjetsCourses[i].Contenu + "," + ListeObjetsCourses[i].Completed + PointVirgule;
        }
        IsolatedStorageSettings.ApplicationSettings["courses"] = StringAEcrire;
        IsolatedStorageSettings.ApplicationSettings.Save();
    }
    public static void EcrireSetting(string Setting, string Content)
    {
        try
        {
            IsolatedStorageSettings.ApplicationSettings.Add(Setting, Content);
        }
        catch (Exception ex)
        {
            IsolatedStorageSettings.ApplicationSettings[Setting] = Content;
        }
    }
    public static int getLastID()
    {
        int Resultat = 0;
        if (ListeObjetsCourses.Count > 0)
        {
            for (int i = 0; i <= ListeObjetsCourses.Count - 1; i++)
            {
                if (ListeObjetsCourses[i].ID > Resultat)
                {
                    Resultat = ListeObjetsCourses[i].ID;
                }
            }
        }
        else
        {
            Resultat = -1;
        }
        return Resultat;
    }
}