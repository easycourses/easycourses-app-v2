﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using BluetoothClientWP8.Resources;
using Windows.Networking.Sockets;
using Windows.Networking.Proximity;
using System.Diagnostics;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using BluetoothConnectionManager;
using System.Windows.Media;

namespace BluetoothClientWP8 //On est dans l'application EasyCourses
{
    public partial class MainPage : PhoneApplicationPage //On est dans la page MainPage 
    {
        private ConnectionManager connectionManager; //Objets du Bluetooth

        private Dictionary<string, string> infosChariot;

        int LastID = 0;



        // Constructeur
        public MainPage()
        {
            infosChariot = new Dictionary<string, string>();
            infosChariot.Add("temperature","0");
            infosChariot.Add("battery", "0");
            infosChariot.Add("time", "0");
            InitializeComponent(); //Charge les éléments de la page
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Default;
            ApplicationBar.Opacity = 1.0;
            ApplicationBar.IsVisible = false;
            ApplicationBar.IsMenuEnabled = true;
            mainPanorama.SelectionChanged += mainPanorama_SelectionChanged;
            connectionManager = new ConnectionManager(); //Initialise les objets du Bluetooth
            connectionManager.MessageReceived += connectionManager_MessageReceived; 
            listeCourses.SelectionChanged += listeCourses_SelectionChanged;
            PublicThings.ChargerListeCourses();
            LastID = PublicThings.getLastID();
            listeCourses.ItemsSource = PublicThings.ListeObjetsCourses;
        }

        private void listeCourses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int SelectedIndex = 0;
            if (e.AddedItems[0] is ElementListeDeCourses)
            {
                ElementListeDeCourses ajoute = (ElementListeDeCourses)e.AddedItems[0];
                SelectedIndex = ajoute.Index;
            }
            PublicThings.ListeObjetsCourses[SelectedIndex].Complete();
            PublicThings.SauvegarderListeCourses();
        }

        private void mainPanorama_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((Panorama)sender).SelectedIndex)
            {
                case 1:
                    if (addingNewElement)
                    { 
                        this.ApplicationBar = new ApplicationBar();
                        ApplicationBarIconButton boutonConfirmerNouvelElement = new ApplicationBarIconButton();
                        boutonConfirmerNouvelElement.IconUri = new Uri("/Images/check.png", UriKind.Relative);
                        boutonConfirmerNouvelElement.Text = "Nouveau";
                        boutonConfirmerNouvelElement.Click += boutonConfirmerNouvelElement_Click;
                        this.ApplicationBar.Buttons.Add(boutonConfirmerNouvelElement);
                        this.ApplicationBar.IsVisible = true;
                    }
                    else
                    {
                        this.ApplicationBar = new ApplicationBar();
                        ApplicationBarIconButton boutonNouvelElement = new ApplicationBarIconButton();
                        boutonNouvelElement.IconUri = new Uri("/Images/add.png", UriKind.Relative);
                        boutonNouvelElement.Text = "Nouveau";
                        boutonNouvelElement.Click += boutonNouvelElement_Click;
                        this.ApplicationBar.Buttons.Add(boutonNouvelElement);
                        this.ApplicationBar.IsVisible = true;
                    }
                    break;
            }
            if (((Panorama)sender).SelectedIndex == 0 || ((Panorama)sender).SelectedIndex == 2 || ((Panorama)sender).SelectedIndex == 3 || ((Panorama)sender).SelectedIndex == 4)
            {
                ApplicationBar.IsVisible = false;
            }
        }
        bool addingNewElement = false;
        private void boutonNouvelElement_Click(object sender, EventArgs e)
        {
            newElementListeDeCourse.Visibility = Visibility.Visible;
            listeCourses.Visibility = Visibility.Collapsed;
            addingNewElement = true;
            this.ApplicationBar = new ApplicationBar();
            ApplicationBarIconButton boutonConfirmerNouvelElement = new ApplicationBarIconButton();
            boutonConfirmerNouvelElement.IconUri = new Uri("/Images/check.png", UriKind.Relative);
            boutonConfirmerNouvelElement.Text = "Nouveau";
            boutonConfirmerNouvelElement.Click += boutonConfirmerNouvelElement_Click;
            this.ApplicationBar.Buttons.Add(boutonConfirmerNouvelElement);
            this.ApplicationBar.IsVisible = true;
            contenuNouvelElement.Focus();
        }

        private void boutonConfirmerNouvelElement_Click(object sender, EventArgs e)
        {
            PublicThings.ListeObjetsCourses.Add(new ElementListeDeCourses(PublicThings.ListeObjetsCourses.Count, contenuNouvelElement.Text, PublicThings.getLastID() + 1, false));
            PublicThings.SauvegarderListeCourses();
            PublicThings.ChargerListeCourses();
            listeCourses.ItemsSource = PublicThings.ListeObjetsCourses;
            contenuNouvelElement.Text = "";

            addingNewElement = false;
            
            this.ApplicationBar = new ApplicationBar();
            ApplicationBarIconButton boutonNouvelElement = new ApplicationBarIconButton();
            boutonNouvelElement.IconUri = new Uri("/Images/add.png", UriKind.Relative);
            boutonNouvelElement.Text = "Nouveau";
            boutonNouvelElement.Click += boutonNouvelElement_Click;
            this.ApplicationBar.Buttons.Add(boutonNouvelElement);
            this.ApplicationBar.IsVisible = true;

            listeCourses.ItemsSource = PublicThings.ListeObjetsCourses;


            listeCourses.Visibility = Visibility.Visible;
            newElementListeDeCourse.Visibility = Visibility.Collapsed;
        }

        String motDePasseEntre = "";
        bool validConnection = false;
        async void connectionManager_MessageReceived(string message)
        {
            Debug.WriteLine("Message reçu :" + message);
            string[] messageArray = message.Split(':');
            switch (messageArray[0])
            {
                case "CAN_I_CONNECT":
                    if (messageArray[1] == "GO")
                    {
                        //validConnection = true;
                        await connectionManager.SendCommand(motDePasseEntre);
                    }
                    else if (messageArray[1] == "PWOK")
                    {
                        //Dispatcher.BeginInvoke(delegate() { MessageBox.Show("Le mot de passe entré est correct ! Appuyez maintenant sur \"Connecté ? Appuyez ici.\"", "Connexion réussie.", MessageBoxButton.OK); });
                        validConnection = true;
                        Dispatcher.BeginInvoke(delegate()
                        {
                            panoramaItemInformations.Visibility = Visibility.Visible;
                            panoramaItemControle.Visibility = Visibility.Visible;
                            panoramaItemConnexion.Visibility = Visibility.Collapsed;
                            BoutonArriere.IsEnabled = true;
                            BoutonAvant.IsEnabled = true;
                            BoutonDroite.IsEnabled = true;
                            BoutonGauche.IsEnabled = true;
                            BoutonStop.IsEnabled = true;
                            boutonReset.IsEnabled = true;
                            this.ApplicationBar = new ApplicationBar();
                            ApplicationBarIconButton boutonNouvelElement = new ApplicationBarIconButton();
                            boutonNouvelElement.IconUri = new Uri("/Images/add.png", UriKind.Relative);
                            boutonNouvelElement.Text = "Nouveau";
                            boutonNouvelElement.Click += boutonNouvelElement_Click;
                            this.ApplicationBar.Buttons.Add(boutonNouvelElement);
                            this.ApplicationBar.IsVisible = true;

                        });
                    }
                    else
                    {
                        Dispatcher.BeginInvoke(delegate() { MessageBox.Show("Le mot de passe entré est incorrect.","Connexion impossible.",MessageBoxButton.OK); });
                        validConnection = false;
                    }
                    break;
                //case "INFORMATION":
                    //infosChariot["information"] = messageArray[1];
                    //break;
                case "BATTERY":
                    infosChariot["battery"] = messageArray[1];
                    Dispatcher.BeginInvoke(delegate() { lblBatterie.Text = getPercentageFromVoltage(infosChariot["battery"]); });
                    break;
                case "TEMPERATURE":
                    infosChariot["temperature"] = messageArray[1];
                    Dispatcher.BeginInvoke(delegate() { lblTemperature.Text = infosChariot["temperature"] + "°C"; });
                    break;
                case "TIME":
                    infosChariot["time"] = messageArray[1];
                    double time = double.Parse(infosChariot["time"]);
                    int hours = 0;
                    int minutes = 0;
                    int seconds = 0;
                    String timeToDisplay = "";
                    if (time >= 3600)
                    {
                        hours = (int)(Math.Floor(time / 3600));
                        time = time % 3600;
                        timeToDisplay = hours + "h";
                    }
                    if (time >= 60)
                    {
                        minutes = (int)(Math.Floor(time / 60));
                        time = time % 60;
                        timeToDisplay += minutes + "min";
                    }
                    seconds = (int)time;
                    timeToDisplay += seconds + "s";
                    Dispatcher.BeginInvoke(delegate() { lblDuree.Text = timeToDisplay; });
                    break;
                default:
                    //Dispatcher.BeginInvoke(delegate() { MessageBox.Show("Une information non homologuée a été reçue : " + message); });
                    break;
            }
        }

        private string getPercentageFromVoltage(string p)
        {
            int currentVoltage = 0;
            if (int.TryParse(p.Split('.')[0], out currentVoltage) == false)
            {
                return "erreur";
            }
            else
            {
                if (currentVoltage > 480)
                    return "> 80%";
                else if (currentVoltage > 450)
                    return "60% ~ 80%";
                else if (currentVoltage > 420)
                    return "40% ~ 60%";
                else if (currentVoltage > 390)
                    return "20% ~ 40%";
                else
                    return "Critique";
            }

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            connectionManager.Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            connectionManager.Terminate();
        }

        private void bConnecter_Click(object sender, RoutedEventArgs e)
        {
            AppToDevice();
        }
        
        private async void AppToDevice()
        {
            bConnecter.Content = "connexion...";
            PeerFinder.AlternateIdentities["Bluetooth:Paired"] = "";
            var pairedDevices = await PeerFinder.FindAllPeersAsync();

            if (pairedDevices.Count == 0)
            {
                Dispatcher.BeginInvoke(delegate() { MessageBox.Show("Le périphérique entré n'est pas appairé aveec le téléphone.", "Erreur", MessageBoxButton.OK); });
            }
            else
            {
                foreach (var pairedDevice in pairedDevices)
                {
                    if (pairedDevice.DisplayName == tbNomChariot.Text)
                    {
                        try
                        {
                            connectionManager.Connect(pairedDevice.HostName);
                            bConnecter.Content = "connecté";
                            tbNomChariot.IsReadOnly = true;
                            bConnecter.IsEnabled = false;
                        }
                        catch (Exception ex)
                        {
                            Dispatcher.BeginInvoke(delegate() { MessageBox.Show("Une erreur s'est produite lors de la connexion au chariot.", "Erreur", MessageBoxButton.OK); });
                        }
                    }
                }
            }
        }

        private async void bValiderConnexion_Click(object sender, RoutedEventArgs e)
        {
            await connectionManager.SendCommand("CAN_I_CONNECT:REQUEST"); //Envoie la commande
            await connectionManager.SendCommand(tbPasswordChariot.Text); //Envoie du mot de passe
        }


        async private void BoutonAvant_Click(object sender, RoutedEventArgs e)
        {
            await connectionManager.SendCommand("MOVEMENT:FORWARD"); //Demande d'avancer
        }

        async private void BoutonDroite_Click(object sender, RoutedEventArgs e)
        {
            await connectionManager.SendCommand("MOVEMENT:RIGHT"); //Demande d'aller à droite
        }

        async private void BoutonGauche_Click(object sender, RoutedEventArgs e)
        {

            await connectionManager.SendCommand("MOVEMENT:LEFT"); //Demande d'aller à gauche
        }

        async private void BoutonArriere_Click(object sender, RoutedEventArgs e)
        {
            await connectionManager.SendCommand("MOVEMENT:BACKWARD"); //Demande d'aller en arrière
        }

        async private void BoutonStop_Click(object sender, RoutedEventArgs e)
        {
            await connectionManager.SendCommand("MOVEMENT:STOP"); //Demande de s'arrêter
        }

        async private void boutonReset_Click(object sender, RoutedEventArgs e)
        {
            await connectionManager.SendCommand("MOVEMENT:DISCONNECT"); //Demande de déconnecter
        }

        private void tbPasswordChariot_TextChanged(object sender, TextChangedEventArgs e)
        {
            motDePasseEntre = tbPasswordChariot.Text; 
        }

        private void lblConnecteAppuyer_Click(object sender, RoutedEventArgs e)
        {
            if (validConnection)
            {
                panoramaItemInformations.Visibility = Visibility.Visible;
                panoramaItemControle.Visibility = Visibility.Visible;
                panoramaItemConnexion.Visibility = Visibility.Collapsed;
                BoutonArriere.IsEnabled = true;
                BoutonAvant.IsEnabled = true;
                BoutonDroite.IsEnabled = true;
                BoutonGauche.IsEnabled = true;
                BoutonStop.IsEnabled = true;
                boutonReset.IsEnabled = true;

            }
            else
            {
                MessageBox.Show("La connexion n'a pas encore été validée. Merci de réessayer.", "Erreur", MessageBoxButton.OK);
            }
        }
    }
}





















