﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
public class ElementListeDeCourses
{
    public string Contenu { get; set; }
    public int ID { get; set; }
    public bool Completed { get; set; }
    public int Index { get; set; }

    public ElementListeDeCourses(int Index, string Contenu, int ID, bool Completed)
    {


        this.Contenu = Contenu;
        this.ID = ID;
        this.Completed = Completed;
        this.Index = Index;
    }
    public void Complete()
    {
        this.Completed = true;
    }

    public void Uncomplete()
    {
        this.Completed = false;
    }
}